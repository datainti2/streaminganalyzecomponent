// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-base-widget',
//   templateUrl: './base-widget.component.html',
//   styleUrls: ['./base-widget.component.css']
// })
// export class BaseWidgetComponent implements OnInit {
//   public dataLoader: Array<any> = [
// 	{
		
// 		"image": "../../assets/default.jpg",
// 		"screen_name": "15 minutes ago",
// 		"timestamp": "Agence France-Presse",
// 		"tweet_text": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//   {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//   {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//    {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
//    {
// 		"title": "China's ZTE pleads guilty to violating US sanction on Iran, N. Korea ",
// 		"image": "../../assets/default.jpg",
// 		"timestamp": "15 minutes ago",
// 		"media_display": "Agence France-Presse",
// 		"description": "Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... Chinese telecom giant ZTEE has pleaded guilty in a US court to violating US export control by... "
// 	},
// ];

//   constructor() {console.log(this.dataLoader);}
  
//   ngOnInit() { 
//   }

// }





import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
  public dataLoader: Array<any> = [
	 {
      "title" : "British Museum show charts America pop art",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malay Mail Online"
    },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malaysian Times"
  },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     },
    {
       "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
    },
    {
     "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     }];

    

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}